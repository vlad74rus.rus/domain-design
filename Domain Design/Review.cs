﻿using System;

namespace Test_Design
{
    public class Review
    {
        public string Message { get; set; }
        public DateTime CurrenTime { get; set; }
        public Player Player { get; set; }
        public Game Game { get; set; }

        public Review(Player player, string message, Game game)
        {
            Message = message;
            CurrenTime = DateTime.Now;
            Player = player;
            Game = game;
        }
    }
}