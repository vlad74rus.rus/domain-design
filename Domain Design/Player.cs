﻿using System;
using System.Collections.Generic;

namespace Test_Design
{
    public class Player
    {
        public int Id { get; private set; }
        public List<Game> Games { get; private set; }
        public decimal Balance { get; private set; }

        public Player()
        {
            Games = new List<Game>();
        }

        public void BuyGame(Game game)
        {
            if (Games.Contains(game))
            {
                throw new InvalidOperationException("Game already exist in User Account");
            }

            if (Balance <= game.Price)
            {
                throw new InvalidOperationException("Balance less then Game Price");
            }

            Games.Add(game);
            Balance -= game.Price;
        }

        public void ReturnGame(Game game)
        {
            if (Games.Contains(game))
            {
                Games.Remove(game);
                Balance += game.Price;
            }
            else
            {
                throw new InvalidOperationException("Такой игры нет у игрока");
            }
        }

        public void GiftGame(Player endPlayer, Game game)
        {
            if (endPlayer.Games.Contains(game))
            {
                throw new InvalidOperationException("Одаряемый игрок уже имеет эту игру");
            }

            if (Balance <= game.Price)
            {
                throw new InvalidOperationException("Balance less then Game Price");
            }

            endPlayer.Games.Add(game);
            Balance -= game.Price;
        }

        public void WriteReview(string message, Game game)
        {
            game.Reviews.Add(new Review(this, message, game));
        }
    }
}