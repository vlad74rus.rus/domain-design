﻿using System;

namespace Test_Design
{
    public class News
    {
        public string Message { get; set; }
        public DateTime CurrenTime { get; set; }
        public GameCompany GameCompany { get; set; }
        public Game Game { get; set; }

        public News(GameCompany gameCompany, string message, Game game)
        {
            Message = message;
            CurrenTime = DateTime.Now;
            GameCompany = gameCompany;
            Game = game;
        }
    }
}