﻿namespace Test_Design.Interfaces
{
    public interface INewsRepository
    {
        void AddNews(News news);
    }
}