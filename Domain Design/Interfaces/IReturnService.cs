﻿namespace Test_Design.Interfaces
{
    public interface IReturnService
    {
        void ReturnGame(Player player, Game game);
    }
}