﻿namespace Test_Design.Interfaces
{
    public interface IUserRepository
    {
        Player Get(int id);
    }
}