﻿namespace Test_Design.Interfaces
{
    public interface IDeliveryService
    {
        void DeliverGame(Game game, Player player);
    }
}