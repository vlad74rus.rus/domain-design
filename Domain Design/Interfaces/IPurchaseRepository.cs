﻿using Domain_Design;

namespace Test_Design.Interfaces
{
    public interface IPurchaseRepository
    {
        Purchase Get(int id);
        void AddPurchase(Purchase purchase);
    }
}