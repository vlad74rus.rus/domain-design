﻿namespace Test_Design.Interfaces
{
    public interface IReviewRepository
    {
        void AddReview(Review review);
    }
}