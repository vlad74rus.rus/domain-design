﻿using System;
using Domain_Design;
using Test_Design.Interfaces;

namespace Test_Design
{
    public class GameService
    {
        private readonly IDeliveryService _deliveryService;
        private readonly IPurchaseRepository _purchaseRepository;
        private readonly IGiftRepository _giftRepository;
        private readonly IGameRepository _gameRepository;
        private readonly INewsRepository _newsRepository;
        private readonly IReviewRepository _reviewRepository;
        private readonly IReturnService _returnService;

        public GameService(IDeliveryService delivery)
        {
            _deliveryService = delivery;
        }

        public void BuyGame(Player player, Game game)
        {
            player.BuyGame(game);
            _purchaseRepository.AddPurchase(new Purchase(player, game));
            _deliveryService.DeliverGame(game, player);
            Console.WriteLine("Покупка прошла успешно");
        }

        public void ReturnGame(Player player, Game game)
        {
            player.ReturnGame(game);
            _returnService.ReturnGame(player, game);
            Console.WriteLine("Возврат прошел успешно");
        }

        public void GiftGame(Player startPlayer, Player endPlayer, Game game)
        {
            startPlayer.GiftGame(endPlayer, game);
            _giftRepository.AddGift(new Gift(startPlayer, endPlayer, game));
            Console.WriteLine("Дарение прошло успешно");
        }

        public void AddReview(Player player, string message, Game game)
        {
            player.WriteReview(message, game);
            _reviewRepository.AddReview(new Review(player, message, game));
            Console.WriteLine("Отзыв добавлен");
        }

        public void AddNews(GameCompany gameCompany, string message, Game game)
        {
            gameCompany.WriteNews(message, game);
            _newsRepository.AddNews(new News(gameCompany, message, game));
            Console.WriteLine("Новость добавлена");
        }
    }
}