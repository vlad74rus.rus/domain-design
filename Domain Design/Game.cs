﻿using System.Collections.Generic;

namespace Test_Design
{
    public class Game
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LinkToFile { get; set; }
        public decimal Price { get; set; }
        public List<Review> Reviews { get; private set; }
        public List<News> News { get; private set; }
    }
}