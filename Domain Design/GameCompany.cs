﻿namespace Test_Design
{
    public class GameCompany
    {
        public string Name { get; set; }

        public GameCompany()
        {
            
        }

        public void WriteNews(string message, Game game)
        {
            game.News.Add(new News(this, message, game));
        }
    }
}