﻿namespace Test_Design
{
    public class Gift
    {
        public int Id { get; private set; }
        public Player StartPlayer { get; private set; }
        public Player EndPlayer { get; private set; }
        public Game Game { get; private set; }

        public Gift(Player startPlayer, Player endPlayer, Game game)
        {
            StartPlayer = startPlayer;
            EndPlayer = endPlayer;
            Game = game;
        }
    }
}