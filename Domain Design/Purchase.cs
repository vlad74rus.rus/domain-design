﻿using System;
using Test_Design;

namespace Domain_Design
{
    public class Purchase
    {
        public int Id { get; private set; }
        public Player Player { get; set; }
        public Game Game { get; set; }
        public DateTime CurrenTime { get; set; }

        public Purchase(Player player, Game game)
        {
            Player = player;
            Game = game;
            CurrenTime = DateTime.Now;
        }
    }
}